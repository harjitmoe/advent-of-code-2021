(lambda lanternghoti_in:
 (lambda functools=__import__("functools"), itertools=__import__("itertools"):
  (lambda bind=(lambda func, seq: tuple(itertools.chain(*map(func, seq)))):
   (lambda lanternghoti_func=(lambda number: (number - 1,) if number > 0 else (6, 8)):
    (lambda part1=print(len(functools.reduce((lambda acc, j: bind(lanternghoti_func, acc)), 
                                             range(80), lanternghoti_in))):
     (lambda price_toupee=tuple(itertools.accumulate(range(16),
                                                     (lambda acc, j: bind(lanternghoti_func, acc)),
                                                     initial=(0,))):
      (lambda after_sixteen=price_toupee[::-1][:9]:
       (lambda counts_after_n_sixteens=tuple(itertools.accumulate(range(16),
                                             (lambda acc, j: tuple(sum(acc[k] for k in i)
                                                             for i in after_sixteen)),
                                             initial=(1,)*9)):
        print(sum(counts_after_n_sixteens[16][i] for i in lanternghoti_in))
       )()
      )()
     )()
    )()
   )()
  )()
 )()
)((1,1,3,1,3,2,1,3,1,1,3,1,1,2,1,3,1,1,3,5,1,1,1,3,1,2,1,1,1,1,4,4,1,2,1,2,1,1,1,5,3,2,1,5,2,5,3,3,2,
2,5,4,1,1,4,4,1,1,1,1,1,1,5,1,2,4,3,2,2,2,2,1,4,1,1,5,1,3,4,4,1,1,3,3,5,5,3,1,3,3,3,1,4,2,2,1,3,4,1,
4,3,3,2,3,1,1,1,5,3,1,4,2,2,3,1,3,1,2,3,3,1,4,2,2,4,1,3,1,1,1,1,1,2,1,3,3,1,2,1,1,3,4,1,1,1,1,5,1,1,
5,1,1,1,4,1,5,3,1,1,3,2,1,1,3,1,1,1,5,4,3,3,5,1,3,4,3,3,1,4,4,1,2,1,1,2,1,1,1,2,1,1,1,1,1,5,1,1,2,1,
5,2,1,1,2,3,2,3,1,3,1,1,1,5,1,1,2,1,1,1,1,3,4,5,3,1,4,1,1,4,1,4,1,1,1,4,5,1,1,1,4,1,3,2,2,1,1,2,3,1,
4,3,5,1,5,1,1,4,5,5,1,1,3,3,1,1,1,1,5,5,3,3,2,4,1,1,1,1,1,5,1,1,2,5,5,4,2,4,4,1,1,3,3,1,5,1,1,1,1,1,1))

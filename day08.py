(lambda rawlines:
 (lambda functools=__import__("functools"), itertools=__import__("itertools"):
  (lambda parseline=(lambda line: (frozenset(frozenset(i) for i in line.split(" | ")[0].split()), tuple(frozenset(i) for i in line.split(" | ")[1].split()))):
   (lambda lines=tuple(map(parseline, rawlines)):
    (lambda find_1478=(lambda combinations: (
        [i for i in combinations if len(i) == 2][0],
        [i for i in combinations if len(i) == 4][0],
        [i for i in combinations if len(i) == 3][0],
        [i for i in combinations if len(i) == 7][0], # Always same but for simplicity
      )):
     (lambda part1=print(sum(len([j for j in i[1] if j in find_1478(i[0])]) for i in lines)):
      (lambda bwire=(lambda combinations:
          [i for i in "abcdefg" if len([j for j in combinations if i in j]) == 6][0]):
       (lambda ewire=(lambda combinations:
           [i for i in "abcdefg" if len([j for j in combinations if i in j]) == 4][0]):
        (lambda fwire=(lambda combinations:
            [i for i in "abcdefg" if len([j for j in combinations if i in j]) == 9][0]):
         (lambda awire=(lambda combinations:
             next(iter(find_1478(combinations)[2] - find_1478(combinations)[0]))):
          (lambda cwire=(lambda combinations:
              [i for i in "abcdefg" if i != awire(combinations) and
                                    len([j for j in combinations if i in j]) == 8][0]):
           (lambda dwire=(lambda combinations:
               [i for i in find_1478(combinations)[1]
                           if len([j for j in combinations if i in j]) == 7][0]):
            (lambda gwire=(lambda combinations:
                [i for i in "abcdefg" if i != dwire(combinations) and
                                      len([j for j in combinations if i in j]) == 7][0]):
             (lambda wiremap=(lambda combinations: {
                 "a": awire(combinations),
                 "b": bwire(combinations),
                 "c": cwire(combinations),
                 "d": dwire(combinations),
                 "e": ewire(combinations),
                 "f": fwire(combinations),
                 "g": gwire(combinations),
               }):
              (lambda digitmap=(lambda combinations:
                  (lambda wires=wiremap(combinations):
                   (lambda toset=(lambda strg: frozenset(wires[i] for i in strg)):
                    {
                     toset("abcefg"): "0",
                     toset("cf"): "1",
                     toset("acdeg"): "2",
                     toset("acdfg"): "3",
                     toset("bcdf"): "4",
                     toset("abdfg"): "5",
                     toset("abdefg"): "6",
                     toset("acf"): "7",
                     toset("abcdefg"): "8",
                     toset("abcdfg"): "9",
                    }
                   )()
                  )()
                ):
               (lambda values=[int("".join(digitmap(combinations)[i] for i in data), 10) for combinations, data in lines]:
                print(sum(values))
               )()
              )()
             )()
            )()
           )()
          )()
         )()
        )()
       )()
      )()
     )()
    )()
   )()
  )()
 )()
)(open("day08.txt").read().splitlines())

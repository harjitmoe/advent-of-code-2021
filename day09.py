(lambda grid:
 (lambda functools=__import__("functools"), itertools=__import__("itertools"):
  (lambda gradients_to_left=(lambda line: tuple(j - i for i, j in itertools.pairwise((10,) + line))):
   (lambda gradients_to_right=(lambda line: tuple(j - i for i, j in itertools.pairwise(line + (10,)))):
    (lambda minima=(lambda line: tuple(
                           n
                           for n, (L, R) in enumerate(zip(gradients_to_left(line), gradients_to_right(line)))
                           if L < 0 and R > 0)):
     (lambda hmins=frozenset(itertools.chain.from_iterable([(i, j) for j in minima(line)]
                                                           for i, line in enumerate(grid))):
      (lambda vmins=frozenset(itertools.chain.from_iterable([(i, j) for i in minima(col)]
                                                            for j, col in enumerate(zip(*grid)))):
       (lambda trumins=hmins & vmins:
        (lambda part1=print(sum(grid[i][j] + 1 for i, j in trumins)):
         (lambda von_neumann_propogate=(lambda coords:
             tuple(((i, j) for i, j in [
                 coords,
                 (coords[0] - 1, coords[1]),
                 (coords[0] + 1, coords[1]),
                 (coords[0], coords[1] - 1),
                 (coords[0], coords[1] + 1),
             ] if 0 <= i < 100 and 0 <= j < 100 and grid[i][j] != 9))
           ):
          (lambda propogate_multiple=(lambda propogate_multiple, subbasin:
              (lambda nextstep=frozenset(itertools.chain(*[von_neumann_propogate(i) for i in subbasin])):
                  nextstep if nextstep == subbasin else propogate_multiple(propogate_multiple, nextstep)
              )()
            ):
           (lambda basins=tuple(sorted([propogate_multiple(propogate_multiple, frozenset({tm})) for tm in trumins], key=len)):
            print(len(basins[-1]), len(basins[-2]), len(basins[-3]), len(basins[-1]) * len(basins[-2]) * len(basins[-3]))
           )()
          )()
         )()
        )()
       )()
      )()
     )()
    )()
   )()
  )()
 )()
)(tuple(tuple(int(j, 10) for j in i) for i in open("day09.txt").read().splitlines()))

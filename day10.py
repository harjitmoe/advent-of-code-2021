(lambda rawlines:
 (lambda functools=__import__("functools"), itertools=__import__("itertools"):
  (lambda Either=type("Either", (object,), {
      "__init__": (lambda self, L, R: [setattr(self, "L", L), setattr(self, "R", R)] and None),
      "has_L": property(lambda self: self.L is not None),
      "has_R": property(lambda self: self.R is not None),
      "__repr__": (lambda self: f"Either({self.L!r}, {self.R!r})"),
    }):
   (lambda step=(lambda step, line, pos, char:
         (lambda acc=[], dat = {"<": ">", "{": "}", "[": "]", "(": ")"},
                 mypos_mut = [pos], stillgoing=[1], stopme=[]:
          [*itertools.takewhile((lambda foo: not stopme), (
           stopme.append(1)
           if not stillgoing else
            stillgoing.clear() or (mypos_mut[-1], Either("T" + char, None))
            if char is not None and mypos_mut[-1] == len(line) else
             stillgoing.clear() or (mypos_mut[-1], Either(None, (char, tuple(acc))))
             if char is None and mypos_mut[-1] == len(line) else
              stillgoing.clear() or ((mypos_mut[-1] + 1), Either(None, (char, tuple(acc))))
              if line[mypos_mut[-1]] == char else
               stillgoing.clear() or (mypos_mut[-1], Either(line[mypos_mut[-1]], None))
               if line[mypos_mut[-1]] not in dat else
                (lambda npb=step(step, line, mypos_mut[-1]+1, dat[line[mypos_mut[-1]]]):
                 (lambda nupos=npb[0], blob=npb[1]:
                  stillgoing.clear() or (nupos, Either(blob.L + (char or "") 
                                                if blob.L[0] == "T" else blob.L, None))
                  if blob.has_L else
                   mypos_mut.append(nupos) or acc.append(blob.R)
                 )()
                )()
           for i in itertools.count())
          )][-1]
         )()
        ):
    (lambda stepped=tuple(step(step, i, 0, None) for i in rawlines):
     (lambda score1=sum({")": 3, "]": 57, "}": 1197, ">": 25137}
                        .get(i[1].L, 0) for i in stepped if i[1].has_L):
      (lambda part1=print(score1):
       (lambda score_complete=(lambda steprow:
           None
           if steprow[1].L[0] != "T" else
           functools.reduce((lambda score, char: (score*5) + "㍘)]}>".index(char)), steprow[1].L[1:], 0)
         ):
        (lambda part2_scores=tuple(i for i in (score_complete(j) for j in stepped) if i != None):
         print(sorted(part2_scores)[int(len(part2_scores)/2)])
        )()
       )()
      )()
     )()
    )()
   )()
  )()
 )()
)(open("day10.txt").read().splitlines())
